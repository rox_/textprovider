package com.textprovider.client;

import java.io.BufferedReader;
import java.io.PrintStream;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class ClientConsoleTest
{
	private static final String GET_MESSAGE = "GET 1";
	private static final String QUIT_MESSAGE = "QUIT";

	@Mock
	private Client client;

	private ClientConsole console;

	@Mock
	private BufferedReader input;
	@Mock
	private PrintStream output;

	@Before
	public void setUp()
	{
		console = new ClientConsole(client);
	}

	@Test
	public void shouldProcessGetMessageAndResultMessage() throws Exception
	{
		// given
		String okStatus = "OK";
		String result = "result";
		when(input.readLine()).thenReturn(GET_MESSAGE, QUIT_MESSAGE);
		when(client.getResponse()).thenReturn(okStatus, result);

		// when
		console.run(input, output);

		// then
		verify(client).send(GET_MESSAGE);
		verify(output).println(okStatus);
		verify(output).println(result);
	}

	@Test
	public void shouldProcessGetMessageWithoutResultMessage() throws Exception
	{
		// given
		String errorStatus = "ERR";
		when(input.readLine()).thenReturn(GET_MESSAGE, QUIT_MESSAGE);
		when(client.getResponse()).thenReturn(errorStatus);

		// when
		console.run(input, output);

		// then
		verify(client).send(GET_MESSAGE);
		verify(output).println(errorStatus);
		verifyNoMoreInteractions(output);
	}

	@Test
	public void shouldProcessSingleQuitMessage() throws Exception
	{
		// given
		when(input.readLine()).thenReturn(QUIT_MESSAGE);

		// when
		console.run(input, output);

		// then
		verify(client, never()).send(anyString());
		verify(client).stop();
	}
}
