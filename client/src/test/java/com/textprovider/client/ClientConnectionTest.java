package com.textprovider.client;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ClientConnectionTest
{
	@Mock
	private Socket socket;
	@Mock
	private BufferedWriter writer;
	@Mock
	private BufferedReader reader;

	@Spy
	private ClientConnection connection = new ClientConnection("host", 1234);

	@Before
	public void setUp() throws UnknownHostException, IOException
	{
		doReturn(socket).when(connection).createSocket();
		doReturn(writer).when(connection).createBufferedWriter(socket);
		doReturn(reader).when(connection).createBufferedReader(socket);
		connection.start();
	}

	@Test
	public void shouldSendMessage() throws IOException
	{
		// given
		String message = "message";

		// when
		connection.send(message);

		// then
		verify(writer).write(message);
	}

	@Test
	public void shouldReturnResponse() throws IOException
	{
		// given
		String response = "response";
		when(reader.readLine()).thenReturn(response);

		// when
		String actual = connection.getResponse();

		// then
		assertEquals(response, actual);
	}

	@Test
	public void shouldStop() throws IOException
	{
		// when
		connection.stop();

		// then
		verify(socket).close();
	}
}
