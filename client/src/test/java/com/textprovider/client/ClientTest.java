package com.textprovider.client;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.UnknownHostException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ClientTest
{
	@Mock
	private ClientConnection connection;

	private Client client;

	@Before
	public void setUp()
	{
		client = new Client(connection);
	}

	@Test
	public void shouldStart() throws UnknownHostException, IOException
	{
		// when
		client.start();

		// then
		verify(connection).start();
	}

	@Test
	public void shouldSendMessage() throws Exception
	{
		// given
		String message = "message";

		// when
		client.send(message);

		// then
		verify(connection).send(message);
	}

	@Test
	public void shouldGetResponse() throws Exception
	{
		// given
		String response = "response";
		Mockito.when(connection.getResponse()).thenReturn(response);

		// when
		String result = client.getResponse();

		// then
		assertEquals(response, result);
	}

	@Test
	public void shouldStop() throws IOException
	{
		// when
		client.stop();

		// then
		verify(connection).stop();
	}
}
