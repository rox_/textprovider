package com.textprovider.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientConnection
{
	private final String host;
	private final int port;

	private Socket socket;
	private BufferedWriter writer;
	private BufferedReader reader;

	private boolean running;

	public ClientConnection(String host, int port)
	{
		this.host = host;
		this.port = port;
	}

	public void start() throws UnknownHostException, IOException
	{
		socket = createSocket();
		writer = createBufferedWriter(socket);
		reader = createBufferedReader(socket);
		running = true;
	}

	Socket createSocket() throws UnknownHostException, IOException
	{
		return new Socket(host, port);
	}

	BufferedWriter createBufferedWriter(Socket socket) throws IOException
	{
		return new BufferedWriter(
		        new OutputStreamWriter(socket.getOutputStream()));
	}

	BufferedReader createBufferedReader(Socket socket) throws IOException
	{
		return new BufferedReader(
		        new InputStreamReader(socket.getInputStream()));
	}

	public void send(String message) throws IOException
	{
		checkIfRunning();
		writer.write(message);
		writer.newLine();
		writer.flush();
	}

	public String getResponse() throws IOException
	{
		checkIfRunning();
		return reader.readLine();
	}

	public void stop() throws IOException
	{
		checkIfRunning();
		socket.close();
	}

	private void checkIfRunning()
	{
		if (!running)
		{
			throw new IllegalStateException("Client connection not started");
		}
	}
}
