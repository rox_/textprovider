package com.textprovider.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientConsole
{
	private static final Logger LOGGER = Logger.getGlobal();

	private final Client client;

	private boolean running = true;

	public ClientConsole(Client client)
	{
		this.client = client;
	}

	public void run(BufferedReader input, PrintStream output) throws IOException
	{
		client.start();

		while (running)
		{
			process(input.readLine(), output);
		}

		client.stop();
	}

	private void process(String message, PrintStream output) throws IOException
	{
		if (ClientControl.QUIT.equals(message))
		{
			running = false;
			return;
		}
		sendMessageByProtocol(message, output);
	}

	private void sendMessageByProtocol(String message, PrintStream output)
	        throws IOException
	{
		client.send(message);
		String status = client.getResponse();
		output.println(status);

		switch (status)
		{
		case Status.OK:
			acceptResult(output);
			break;
		case Status.ERROR:
			break;
		default:
			LOGGER.log(Level.WARNING, "Unsupported status");
		}
	}

	private void acceptResult(PrintStream output) throws IOException
	{
		String response = client.getResponse();
		output.println(response);
	}

	private static class ClientControl
	{
		static final String QUIT = "QUIT";
	}

	private static class Status
	{
		static final String OK = "OK";
		static final String ERROR = "ERR";
	}
}
