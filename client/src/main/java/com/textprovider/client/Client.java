package com.textprovider.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client
{
	private static final Logger LOGGER = Logger.getGlobal();

	private ClientConnection connection;

	public Client(ClientConnection connection)
	{
		this.connection = connection;
	}

	public void start() throws UnknownHostException, IOException
	{
		connection.start();
	}

	public void send(String message) throws IOException
	{
		connection.send(message);
	}

	public String getResponse() throws IOException
	{
		return connection.getResponse();
	}

	public void stop() throws IOException
	{
		connection.stop();
	}

	public static void main(String[] args)
	{
		ClientConnection connection = new ClientConnection("localhost", 10322);
		ClientConsole console = new ClientConsole(new Client(connection));

		try
		{
			runConsole(console);
		}
		catch (ConnectException e)
		{
			LOGGER.log(Level.INFO, "No server available");
		}
		catch (SocketException e)
		{
			LOGGER.log(Level.INFO, "Connection closed");
		}
		catch (IOException e)
		{
			LOGGER.log(Level.SEVERE, "Client run interrupted", e);
		}
	}

	private static void runConsole(ClientConsole console) throws IOException
	{
		try (BufferedReader systemIn = new BufferedReader(
		        new InputStreamReader(System.in)))
		{
			console.run(systemIn, System.out);
		}
	}
}
