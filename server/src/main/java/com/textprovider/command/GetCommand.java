package com.textprovider.command;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Optional;

import com.textprovider.server.ServerResponse;
import com.textprovider.server.textprovider.CachedTextProvider;

public class GetCommand extends AbstractCommand
{
	private final long lineNumber;

	GetCommand(long lineNumber)
	{
		this.lineNumber = lineNumber;
	}

	@Override
	public void execute(CachedTextProvider textProvider, BufferedWriter writer)
	        throws IOException
	{
		Optional<String> line = textProvider.getLine(lineNumber);

		if (line.isPresent())
		{
			write(writer, ServerResponse.OK);
			write(writer, line.get());
		}
		else
		{
			write(writer, ServerResponse.ERROR);
		}
	}
}
