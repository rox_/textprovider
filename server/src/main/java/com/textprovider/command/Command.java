package com.textprovider.command;

import java.io.BufferedWriter;
import java.io.IOException;
import com.textprovider.server.textprovider.CachedTextProvider;

public interface Command
{
	void execute(CachedTextProvider textProvider, BufferedWriter writer)
	        throws IOException;
}
