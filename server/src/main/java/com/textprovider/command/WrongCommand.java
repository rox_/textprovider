package com.textprovider.command;

import java.io.BufferedWriter;
import java.io.IOException;
import com.textprovider.server.ServerResponse;
import com.textprovider.server.textprovider.CachedTextProvider;

public class WrongCommand extends AbstractCommand
{
	@Override
	public void execute(CachedTextProvider textProvider, BufferedWriter writer)
	        throws IOException
	{
		write(writer, ServerResponse.ERROR);
	}
}
