package com.textprovider.command;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class CommandFactory
{
	private static final Pattern GET_COMMAND = Pattern.compile("GET \\d+");

	public static Command getCommand(String input)
	{
		try
		{
			if (GET_COMMAND.matcher(input).matches())
			{
				long lineNumber = Long.valueOf(input.substring(4));
				return new GetCommand(lineNumber);
			}
		}
		catch (NumberFormatException e)
		{
			Logger.getGlobal().log(Level.INFO, "Unparsable number");
		}
		return new WrongCommand();
	}
}
