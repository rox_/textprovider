package com.textprovider.command;

import java.io.BufferedWriter;
import java.io.IOException;

import com.textprovider.server.ServerResponse;

public abstract class AbstractCommand implements Command
{
	protected void write(BufferedWriter writer, ServerResponse response)
	        throws IOException
	{
		write(writer, response.toString());
	}

	protected void write(BufferedWriter writer, String message)
	        throws IOException
	{
		writer.write(message);
		writer.newLine();
		writer.flush();
	}
}
