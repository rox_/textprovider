package com.textprovider.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SocketAccessor
{
	private static final Logger LOGGER = Logger.getGlobal();

	private ServerSocket serverSocket;
	private Set<RunnableOnSocket> activeRunnables;

	public SocketAccessor(ServerSocket serverSocket) throws IOException
	{
		this.serverSocket = serverSocket;
		this.activeRunnables = new HashSet<>();
	}

	public Socket acceptNewSocket() throws IOException
	{
		// deliberately non-synchronized
		try
		{
			return serverSocket.accept();
		}
		catch (SocketException e)
		{
			LOGGER.log(Level.INFO, "Shutting down server connection");
			return null;
		}
	}

	public synchronized void addRunnableOnSocket(RunnableOnSocket runnable)
	        throws IOException
	{
		if (serverSocket.isClosed())
		{
			runnable.close();
		}
		else
		{
			activeRunnables.add(runnable);
		}
	}

	public synchronized void close() throws IOException
	{
		serverSocket.close();
		for (RunnableOnSocket runnable : activeRunnables)
		{
			runnable.close();
		}
	}
}
