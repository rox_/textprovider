package com.textprovider.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerConnection implements Runnable
{
	private static final Logger LOGGER = Logger.getGlobal();

	private final int port;
	private SocketAccessor socketAccessor;

	private RunnableOnSocket runnableOnSocket;

	public ServerConnection(int port)
	{
		this.port = port;
	}

	public void setRunnableOnSocket(RunnableOnSocket runnable)
	{
		this.runnableOnSocket = runnable;
	}

	public void run()
	{
		try
		{
			setSocketAccessor();
			Socket socket;
			while ((socket = socketAccessor.acceptNewSocket()) != null)
			{
				RunnableOnSocket r = createRunnable(socket);
				socketAccessor.addRunnableOnSocket(r);
				createThread(r).start();
			}
		}
		catch (IOException e)
		{
			LOGGER.log(Level.SEVERE,
			        "Server connection interrupted with exception", e);
		}
		catch (CloneNotSupportedException e)
		{
			LOGGER.log(Level.SEVERE,
			        "Server connection interrupted with exception", e);
		}
	}

	private synchronized void setSocketAccessor() throws IOException
	{
		socketAccessor = createSocketAccessor(port);
	}

	SocketAccessor createSocketAccessor(int port) throws IOException
	{
		return new SocketAccessor(new ServerSocket(port));
	}

	private RunnableOnSocket createRunnable(Socket socket)
	        throws CloneNotSupportedException, IOException
	{
		RunnableOnSocket r = runnableOnSocket.clone();
		r.setSocket(socket);
		return r;
	}

	Thread createThread(Runnable r)
	{
		return new Thread(r);
	}

	public synchronized void stop() throws IOException
	{
		if (socketAccessor == null)
		{
			LOGGER.log(Level.SEVERE, "Cannot stop non-running connection");
			return;
		}
		socketAccessor.close();
	}
}
