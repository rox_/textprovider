package com.textprovider.server;

import java.io.Serializable;

public enum ServerResponse implements Serializable
{
	OK("OK"), ERROR("ERR");

	private final String text;

	private ServerResponse(String text)
	{
		this.text = text;
	}

	@Override
	public String toString()
	{
		return text;
	}
}
