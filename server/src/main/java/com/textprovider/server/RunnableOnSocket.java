package com.textprovider.server;

import java.io.IOException;
import java.net.Socket;

public interface RunnableOnSocket extends Runnable, Cloneable
{
	public void setSocket(Socket socket) throws IOException;

	public void close();

	public RunnableOnSocket clone() throws CloneNotSupportedException;
}
