package com.textprovider.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerConsole
{
	private static final Logger LOGGER = Logger.getGlobal();

	private final Server server;
	private boolean running = true;

	public ServerConsole(Server server)
	{
		this.server = server;
	}

	public void run(BufferedReader input) throws IOException
	{
		server.start();

		while (running)
		{
			process(input.readLine());
		}

		server.stop();
	}

	private void process(String message)
	{
		if (ServerControl.SHUTDOWN.equals(message))
		{
			running = false;
			return;
		}
		else
		{
			LOGGER.log(Level.WARNING, "Unsupported operation");
		}
	}

	private class ServerControl
	{
		static final String SHUTDOWN = "SHUTDOWN";
	}
}
