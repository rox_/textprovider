package com.textprovider.server.textprovider;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Optional;

public class TextProvider
{
	private static final String EMPTY_STRING = "";
	private static final int SKIP_DISTANCE = 10;

	private RandomAccessFile randomAccessFile;
	private FileInfo fileInfo;

	public TextProvider(File file) throws IOException
	{
		randomAccessFile = new RandomAccessFile(file, "r");
		fileInfo = new FileInfo(randomAccessFile, SKIP_DISTANCE);
	}

	public Optional<String> getLine(long lineNumber) throws IOException
	{
		if (fileInfo.isInvalidLineNumber(lineNumber))
		{
			return Optional.empty();
		}

		synchronized (this)
		{
			// translation to 0-starting numeration
			setOffset(lineNumber - 1);
			return Optional
			        .of(getEmptyStringIfNull(randomAccessFile.readLine()));
		}
	}

	private void setOffset(long lineNumber) throws IOException
	{
		randomAccessFile.seek(fileInfo.getFloorOffset(lineNumber));
		shiftBy(lineNumber % SKIP_DISTANCE);
	}

	private void shiftBy(long lineDistance) throws IOException
	{
		for (int i = 0; i < lineDistance; i++)
		{
			randomAccessFile.readLine();
		}
	}

	private static String getEmptyStringIfNull(String line)
	{
		return (line == null) ? EMPTY_STRING : line;
	}

	public synchronized void close() throws IOException
	{
		randomAccessFile.close();
	}
}
