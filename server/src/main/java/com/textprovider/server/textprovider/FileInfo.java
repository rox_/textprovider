package com.textprovider.server.textprovider;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Collections;
import java.util.NavigableMap;
import java.util.TreeMap;

public class FileInfo
{
	private final long skipDistance;
	private NavigableMap<Long, Long> lineOffsets;
	private long lines;

	FileInfo(RandomAccessFile file, long skipDistance) throws IOException
	{
		this.skipDistance = skipDistance;
		collectLineOffsets(file);
	}

	private void collectLineOffsets(RandomAccessFile file) throws IOException
	{
		NavigableMap<Long, Long> lineOffsets = new TreeMap<>();

		long lineNumber = 0;
		do
		{
			if (lineNumber % skipDistance == 0)
			{
				lineOffsets.put(lineNumber, file.getFilePointer());
			}
			lineNumber++;
		}
		while (file.readLine() != null);

		this.lines = lineNumber;
		this.lineOffsets = Collections.unmodifiableNavigableMap(lineOffsets);
	}

	boolean isInvalidLineNumber(long ln)
	{
		return ln < 1 || ln > lines;
	}

	long getFloorOffset(long lineNumber)
	{
		return lineOffsets.floorEntry(lineNumber).getValue();
	}
}
