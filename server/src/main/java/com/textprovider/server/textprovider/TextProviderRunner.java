package com.textprovider.server.textprovider;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.textprovider.command.Command;
import com.textprovider.command.CommandFactory;
import com.textprovider.server.RunnableOnSocket;

public class TextProviderRunner implements RunnableOnSocket
{
	private static final Logger LOGGER = Logger.getGlobal();

	private CachedTextProvider textProvider;

	private Socket socket;
	private BufferedReader reader;
	private BufferedWriter writer;

	public TextProviderRunner(CachedTextProvider textProvider)
	{
		this.textProvider = textProvider;
	}

	@Override
	public void setSocket(Socket socket) throws IOException
	{
		this.socket = socket;
		this.reader = getBufferedReader(socket);
		this.writer = getBufferedWriter(socket);
	}

	BufferedReader getBufferedReader(Socket socket) throws IOException
	{
		return new BufferedReader(
		        new InputStreamReader(socket.getInputStream()));
	}

	BufferedWriter getBufferedWriter(Socket socket) throws IOException
	{
		return new BufferedWriter(
		        new OutputStreamWriter(socket.getOutputStream()));
	}

	@Override
	public void run()
	{
		try
		{
			Command command;
			while ((command = getCommand(reader.readLine())) != null)
			{
				command.execute(textProvider, writer);
			}
		}
		catch (SocketException e)
		{
			LOGGER.log(Level.INFO, "Connection closed");
		}
		catch (IOException e)
		{
			LOGGER.log(Level.SEVERE, "Stream reading interrupted", e);
		}
		finally
		{
			close();
		}
	}

	Command getCommand(String line)
	{
		if (line == null)
		{
			return null;
		}
		return CommandFactory.getCommand(line);
	}

	@Override
	public void close()
	{
		try
		{
			socket.close();
		}
		catch (IOException e)
		{
			LOGGER.log(Level.SEVERE, "Socket closing interrupted");
		}
	}

	@Override
	public TextProviderRunner clone() throws CloneNotSupportedException
	{
		return (TextProviderRunner) super.clone();
	}
}
