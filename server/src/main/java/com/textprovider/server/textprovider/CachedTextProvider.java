package com.textprovider.server.textprovider;

import java.io.IOException;
import java.util.Optional;

public class CachedTextProvider implements AutoCloseable
{
	private TextProvider textProvider;
	private FileCache fileCache;

	public CachedTextProvider(TextProvider textProvider)
	{
		this.textProvider = textProvider;
		this.fileCache = new FileCache();
	}

	public Optional<String> getLine(long lineNumber) throws IOException
	{
		Optional<String> cachedLine = fileCache.getLine(lineNumber);
		if (cachedLine.isPresent())
		{
			return cachedLine;
		}

		Optional<String> line = textProvider.getLine(lineNumber);
		fileCache.saveLine(lineNumber, line);
		return line;
	}

	@Override
	public void close() throws IOException
	{
		textProvider.close();
	}
}
