package com.textprovider.server.textprovider;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class FileCache
{
	private Map<Long, SoftReference<String>> cache = new HashMap<>();

	Optional<String> getLine(long lineNumber)
	{
		if (!containsLine(lineNumber))
		{
			return Optional.empty();
		}
		return Optional.ofNullable(cache.get(lineNumber).get());
	}

	synchronized void saveLine(long lineNumber, Optional<String> line)
	{
		if (line.isPresent() && !containsLine(lineNumber))
		{
			cache.put(lineNumber, new SoftReference<String>(line.get()));
		}
	}

	private synchronized boolean containsLine(long lineNumber)
	{
		return cache.containsKey(lineNumber)
		        && cache.get(lineNumber).get() != null;
	}
}
