package com.textprovider.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.textprovider.server.textprovider.CachedTextProvider;
import com.textprovider.server.textprovider.TextProvider;
import com.textprovider.server.textprovider.TextProviderRunner;

public class Server
{
	private static final Logger LOGGER = Logger.getGlobal();

	private CachedTextProvider textProvider;
	private ServerConnection connection;

	public Server(ServerConnection sc, CachedTextProvider textProvider)
	{
		this.connection = sc;
		this.textProvider = textProvider;
	}

	public void start() throws IOException
	{
		connection.setRunnableOnSocket(createTextProviderRunner());
		startThread(connection);
	}

	private RunnableOnSocket createTextProviderRunner()
	{
		return new TextProviderRunner(textProvider);
	}

	void startThread(Runnable r)
	{
		new Thread(r).start();
	}

	public void stop() throws IOException
	{
		connection.stop();
	}

	public static void main(String[] args)
	{
		try
		{
			LOGGER.log(Level.INFO, "Loading file...");
			try (CachedTextProvider textProvider = getCachedTextProvider(
			        args[0]))
			{
				LOGGER.log(Level.INFO, "File loaded");

				Server server = new Server(new ServerConnection(10322),
				        textProvider);
				ServerConsole console = new ServerConsole(server);

				runConsole(console);
			}
		}
		catch (FileNotFoundException e)
		{
			LOGGER.log(Level.SEVERE,
			        "Could not start the server: file not found");
		}
		catch (IOException e)
		{
			LOGGER.log(Level.SEVERE, "Server run interrupted", e);
		}
	}

	private static CachedTextProvider getCachedTextProvider(String pathname)
	        throws IOException
	{
		return new CachedTextProvider(new TextProvider(new File(pathname)));
	}

	private static void runConsole(ServerConsole console) throws IOException
	{
		try (BufferedReader systemIn = new BufferedReader(
		        new InputStreamReader(System.in)))
		{
			console.run(systemIn);
		}
	}
}
