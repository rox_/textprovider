package com.textprovider.server;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import com.textprovider.server.textprovider.CachedTextProvider;

@RunWith(MockitoJUnitRunner.class)
public class ServerTest
{
	@Mock
	private ServerConnection connection;
	@Mock
	private CachedTextProvider textProvider;

	private Server server;

	@Before
	public void setUp()
	{
		server = spy(new Server(connection, textProvider));
		doNothing().when(server).startThread(any(Runnable.class));
	}

	@Test
	public void shouldRunServer() throws IOException
	{
		// when
		server.start();

		// given
		verify(connection).setRunnableOnSocket(any(RunnableOnSocket.class));
		verify(server).startThread(any(Runnable.class));
	}
}
