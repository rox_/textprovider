package com.textprovider.server;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SocketAccessorTest
{
	@Mock
	private Socket socket;
	@Mock
	private ServerSocket serverSocket;

	@Mock
	private RunnableOnSocket runnable;

	private SocketAccessor socketAccessor;

	@Before
	public void setUp() throws IOException
	{
		socketAccessor = new SocketAccessor(serverSocket);
	}

	@Test
	public void shouldAcceptNewSocket() throws IOException
	{
		// given
		when(serverSocket.accept()).thenReturn(socket);

		// when
		Socket s = socketAccessor.acceptNewSocket();

		// then
		assertSame(socket, s);
	}

	@Test
	public void shouldReturnNullWhenServerSocketClosed() throws IOException
	{
		// given
		when(serverSocket.accept()).thenThrow(SocketException.class);

		// when
		Socket s = socketAccessor.acceptNewSocket();

		// then
		assertNull(s);
	}

	@Test
	public void shouldCloseRunnableInsteadOfSavingIfServerSocketClosed()
	        throws IOException
	{
		// given
		when(serverSocket.isClosed()).thenReturn(true);

		// when
		socketAccessor.addRunnableOnSocket(runnable);

		// then
		verify(runnable).close();
	}

	@Test
	public void shouldCloseAllSockets() throws IOException
	{
		// given
		socketAccessor.addRunnableOnSocket(runnable);
		RunnableOnSocket runnable2 = mock(RunnableOnSocket.class);
		socketAccessor.addRunnableOnSocket(runnable2);

		// when
		socketAccessor.close();

		// then
		verify(serverSocket).close();
		verify(runnable).close();
		verify(runnable2).close();
	}
}
