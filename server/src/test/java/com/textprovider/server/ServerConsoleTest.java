package com.textprovider.server;

import java.io.BufferedReader;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ServerConsoleTest
{
	private static final String SHUTDOWN_MESSAGE = "SHUTDOWN";

	@Mock
	private BufferedReader input;
	@Mock
	private Server server;

	private ServerConsole console;

	@Before
	public void setUp()
	{
		console = new ServerConsole(server);
	}

	@Test
	public void shouldShutdownOnCommand() throws IOException
	{
		// given
		when(input.readLine()).thenReturn("otherMessage", SHUTDOWN_MESSAGE);

		// when
		console.run(input);

		// then
		verify(server).start();
		verify(input, times(2)).readLine();
		verify(server).stop();
	}
}
