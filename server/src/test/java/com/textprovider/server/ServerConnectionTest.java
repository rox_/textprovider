package com.textprovider.server;

import java.io.IOException;
import java.net.Socket;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ServerConnectionTest
{
	private static final int PORT = 1234;

	@Mock
	private SocketAccessor socketAccessor;
	@Mock
	private Socket socket;

	@Spy
	private ServerConnection serverConnection = new ServerConnection(PORT);

	@Mock
	private RunnableOnSocket runnable;
	@Mock
	private RunnableOnSocket clonedRunnable;
	@Mock
	private Thread thread;

	@Before
	public void setUp() throws IOException, CloneNotSupportedException
	{
		serverConnection.setRunnableOnSocket(runnable);
		when(runnable.clone()).thenReturn(clonedRunnable);

		when(serverConnection.createSocketAccessor(PORT))
		        .thenReturn(socketAccessor);
		when(serverConnection.createThread(clonedRunnable)).thenReturn(thread);
	}

	@Test
	public void shouldStartThreadPerSocket() throws IOException
	{
		// given
		when(socketAccessor.acceptNewSocket()).thenReturn(socket, null);

		// when
		serverConnection.run();

		// then
		verify(clonedRunnable).setSocket(socket);
		verify(thread).start();
	}
}
