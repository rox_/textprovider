package com.textprovider.server.textprovider;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.Socket;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.textprovider.command.Command;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TextProviderRunnerTest
{
	@Mock
	private Socket socket;
	@Mock
	private BufferedReader reader;
	@Mock
	private BufferedWriter writer;
	@Mock
	private CachedTextProvider textProvider;

	private TextProviderRunner textProviderRunner;

	@Before
	public void setUp() throws IOException
	{
		textProviderRunner = Mockito.spy(new TextProviderRunner(textProvider));

		doReturn(reader).when(textProviderRunner).getBufferedReader(socket);
		doReturn(writer).when(textProviderRunner).getBufferedWriter(socket);
		textProviderRunner.setSocket(socket);
	}

	@Test
	public void shouldProcessInputMessage() throws IOException
	{
		// given
		String line = "line";
		when(reader.readLine()).thenReturn(line, null);

		Command message = mock(Command.class);
		when(textProviderRunner.getCommand(line)).thenReturn(message);

		// when
		textProviderRunner.run();

		// then
		verify(message).execute(textProvider, writer);
	}
}
