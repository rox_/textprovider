package com.textprovider.server.textprovider;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CachedTextProviderTest
{
	@Mock
	private TextProvider textProvider;
	@Mock
	private FileCache fileCache;

	@InjectMocks
	private CachedTextProvider cachedTextProvider = new CachedTextProvider(
	        textProvider);

	@Test
	public void shouldReturnCachedLine() throws Exception
	{
		// given
		long lineNumber = 123;
		String line = "cachedLine";
		when(fileCache.getLine(lineNumber)).thenReturn(Optional.of(line));

		// when
		Optional<String> result = cachedTextProvider.getLine(lineNumber);

		// then
		assertEquals(line, result.get());
	}

	@Test
	public void shouldCacheAndReturnNewLine() throws Exception
	{
		// given
		long lineNumber = 123;
		String line = "nonCachedLine";
		when(fileCache.getLine(lineNumber)).thenReturn(Optional.empty());
		when(textProvider.getLine(lineNumber)).thenReturn(Optional.of(line));

		// when
		Optional<String> result = cachedTextProvider.getLine(lineNumber);

		// then
		verify(fileCache).saveLine(lineNumber, result);
		assertEquals(line, result.get());
	}
}
