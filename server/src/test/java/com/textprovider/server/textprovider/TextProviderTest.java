package com.textprovider.server.textprovider;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Optional;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.spy;

@RunWith(MockitoJUnitRunner.class)
public class TextProviderTest
{
	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	private File file;

	private TextProvider textProvider;

	@Before
	public void setUp() throws IOException
	{
		file = spy(createTestFile());
		textProvider = new TextProvider(file);
	}

	private File createTestFile() throws IOException
	{
		File file = tempFolder.newFile("testFile");
		BufferedWriter bf = new BufferedWriter(new FileWriter(file));
		bf.write("1st\n");
		bf.write("2nd\n");
		bf.write("3rd\n");
		bf.write("4th\n");
		bf.write("5th\n");
		bf.write("6th\n");
		bf.write("7th\n");
		bf.write("8th\n");
		bf.write("9th\n");
		bf.write("10th\n");
		bf.write("11th\n");
		bf.write("12th\n");
		bf.write("13th\n");
		bf.write("14th\n");
		bf.write("15th\n");
		bf.close();
		return file;
	}

	@Test
	public void shouldReturnFirstLine() throws IOException
	{
		// when
		Optional<String> result = textProvider.getLine(1);

		// then
		assertEquals("1st", result.get());
	}

	@Test
	public void shouldReturnLineWithSimpleOffset() throws IOException
	{
		// when
		Optional<String> result = textProvider.getLine(11);

		// then
		assertEquals("11th", result.get());
	}

	@Test
	public void shouldReturnLineByComplexOffset() throws Exception
	{
		// when
		Optional<String> result = textProvider.getLine(13);

		// then
		assertEquals("13th", result.get());
	}

	@Test
	public void shouldReturnLastLine() throws IOException
	{
		// when
		Optional<String> result = textProvider.getLine(15);

		// then
		assertEquals("15th", result.get());
	}
}
