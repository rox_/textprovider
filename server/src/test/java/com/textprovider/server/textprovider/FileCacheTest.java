package com.textprovider.server.textprovider;

import static org.junit.Assert.*;

import java.lang.ref.SoftReference;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FileCacheTest
{
	@Mock
	private ConcurrentHashMap<Long, SoftReference<String>> cache;

	@InjectMocks
	private FileCache fileCache;

	@Test
	public void shouldReturnCachedLine()
	{
		// given
		long lineNumber = 123;
		String line = "line";
		Mockito.when(cache.containsKey(lineNumber)).thenReturn(true);
		Mockito.when(cache.get(lineNumber))
		        .thenReturn(new SoftReference<String>(line));

		// when
		Optional<String> actual = fileCache.getLine(lineNumber);

		// then
		assertEquals(line, actual.get());
	}

	@Test
	public void shouldReturnEmptyOptionalIfLineNotCached()
	{
		// given
		long lineNumber = 321;
		Mockito.when(cache.containsKey(lineNumber)).thenReturn(false);

		// when
		Optional<String> line = fileCache.getLine(lineNumber);

		// then
		assertFalse(line.isPresent());
	}

	@Test
	public void shouldReturnEmptyOptionalIfLineGarbageCollected()
	{
		// given
		long lineNumber = 321;
		Mockito.when(cache.containsKey(lineNumber)).thenReturn(true);
		Mockito.when(cache.get(lineNumber))
		        .thenReturn(new SoftReference<String>(null));

		// when
		Optional<String> line = fileCache.getLine(lineNumber);

		// then
		assertFalse(line.isPresent());
	}
}
