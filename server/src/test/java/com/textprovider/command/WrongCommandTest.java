package com.textprovider.command;

import java.io.BufferedWriter;
import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import com.textprovider.server.textprovider.CachedTextProvider;

@RunWith(MockitoJUnitRunner.class)
public class WrongCommandTest
{
	@Mock
	private CachedTextProvider textProvider;
	@Mock
	private BufferedWriter writer;

	private WrongCommand wrongCommand = new WrongCommand();

	@Test
	public void shouldSendError() throws IOException
	{
		// when
		wrongCommand.execute(textProvider, writer);

		// then
		verifyZeroInteractions(textProvider);
		verify(writer).write("ERR");
	}
}
