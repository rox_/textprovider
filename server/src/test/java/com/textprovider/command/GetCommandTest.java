package com.textprovider.command;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.textprovider.server.textprovider.CachedTextProvider;

@RunWith(MockitoJUnitRunner.class)
public class GetCommandTest
{
	private static final long LINE_NUMBER = 123L;

	@Mock
	private CachedTextProvider textProvider;
	@Mock
	private BufferedWriter writer;

	private GetCommand getCommand = new GetCommand(LINE_NUMBER);

	@Test
	public void shouldSendTheLine() throws IOException
	{
		// given
		String line = "line";
		when(textProvider.getLine(LINE_NUMBER)).thenReturn(Optional.of(line));

		// when
		getCommand.execute(textProvider, writer);

		// then
		verify(writer).write("OK");
		verify(writer).write(line);
	}

	@Test
	public void shouldSendError() throws IOException
	{
		// given
		when(textProvider.getLine(LINE_NUMBER)).thenReturn(Optional.empty());

		// when
		getCommand.execute(textProvider, writer);

		// then
		verify(writer).write("ERR");
	}

	@Test(expected = IOException.class)
	public void shouldThrowIOException() throws IOException
	{
		// given
		when(textProvider.getLine(LINE_NUMBER)).thenThrow(IOException.class);

		// when
		getCommand.execute(textProvider, writer);
	}
}
